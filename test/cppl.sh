# for copying from bum3u to m3u
M3USDIR=./bum3u
TESTDIR=./m3u

cp -pv $M3USDIR/actual_reenactment.m3u $TESTDIR/treepeople/actual_reenactment/actual_reenactment.m3u
cp -pv $M3USDIR/verve_jazz_masters_9.m3u $TESTDIR/astrud_gilberto/verve_jazz_masters_9.m3u
cp -pv $M3USDIR/american_recordings.m3u $TESTDIR/johnny_cash/american_recordings/american_recordings.m3u
cp -pv $M3USDIR/who_is_this_america.m3u $TESTDIR/antibalas/who_is_this_america/who_is_this_america.m3u
cp -pv $M3USDIR//enesco__liszt_roumanian__hungarian_rhapsodies.m3u $TESTDIR/antal_dorati_london_symphony_orchestra/enesco__liszt_roumanian__hungarian_rhapsodies/enesco__liszt_roumanian__hungarian_rhapsodies.m3u
cp -pv $M3USDIR/a_night_in_tunisia.m3u $TESTDIR/art_blakey_and_the_jazz_messen/a_night_in_tunisia/a_night_in_tunisia.m3u


# for copying the original m3u's from a mounted drive
# M3USDIR=$1
# TESTDIR=$2
# 
# cp -pv $M3USDIR/treepeople/actual_reenactment/actual_reenactment.m3u $TESTDIR/treepeople/actual_reenactment/actual_reenactment.m3u
# cp -pv $M3USDIR/astrud_gilberto/verve_jazz_masters_9.m3u $TESTDIR/astrud_gilberto/verve_jazz_masters_9.m3u
# cp -pv $M3USDIR/johnny_cash/american_recordings/american_recordings.m3u $TESTDIR/johnny_cash/american_recordings/american_recordings.m3u
# cp -pv $M3USDIR/antibalas/who_is_this_america/who_is_this_america.m3u $TESTDIR/antibalas/who_is_this_america/who_is_this_america.m3u
# cp -pv $M3USDIR/antal_dorati_london_symphony_orchestra/enesco__liszt_roumanian__hungarian_rhapsodies/enesco__liszt_roumanian__hungarian_rhapsodies.m3u $TESTDIR/antal_dorati_london_symphony_orchestra/enesco__liszt_roumanian__hungarian_rhapsodies/enesco__liszt_roumanian__hungarian_rhapsodies.m3u
# cp -pv $M3USDIR/art_blakey_and_the_jazz_messen/a_night_in_tunisia/a_night_in_tunisia.m3u $TESTDIR/art_blakey_and_the_jazz_messen/a_night_in_tunisia/a_night_in_tunisia.m3u
